import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:wifi_iot/wifi_iot.dart';
import 'package:wifi_scan/wifi_scan.dart';

import 'IdPassword.dart';

class Configuration extends StatefulWidget {
  const Configuration({Key? key}) : super(key: key);

  @override
  State<Configuration> createState() => _ConfigurationState();
}

class _ConfigurationState extends State<Configuration> {

  TextEditingController configController = TextEditingController();

  String combined = "";

  List<WiFiAccessPoint> accessPoints = <WiFiAccessPoint>[];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "InfyIOT"
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Enter the Mac Address",
                  labelText: "Mac Address"
                ),
                controller: configController,
              ),
              SizedBox(height: 20),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  onPressed: () async {
                    combined = "INFYIOT${configController.text}";
                    print(combined);
                    final error = await WiFiScan.instance.startScan();
                    print("${error ?? 'Done'}");
                    List ssiList = [];
                    WiFiScan.instance.getScannedResults().then((value) => setState(() {
                      accessPoints = value.value!;
                      print(accessPoints);
                      ssiList.clear();
                      for(int i=0;i<accessPoints.length;i++){
                        ssiList.add(accessPoints[i].ssid);
                      }
                      print(ssiList);
                      for(int j =0;j<ssiList.length;j++){
                        if(ssiList[j]==combined){
                          WiFiForIoTPlugin.connect(
                              ssiList[j],
                              password: "1122334455",
                              joinOnce: true,
                              security: NetworkSecurity.WPA
                          ).then((value) => Navigator.push(context, MaterialPageRoute(builder: (context) => IdPassword())));
                          print("Connecting state");
                        }
                      }
                    }));
                    //
                  },
                  child: Text(
                    "Next"
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
