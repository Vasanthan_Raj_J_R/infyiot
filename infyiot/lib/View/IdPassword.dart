import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:http/retry.dart';
import 'package:wifi_iot/wifi_iot.dart';
import 'package:wifi_scan/wifi_scan.dart';

class IdPassword extends StatefulWidget {
  const IdPassword({Key? key}) : super(key: key);

  @override
  State<IdPassword> createState() => _IdPasswordState();
}

class _IdPasswordState extends State<IdPassword> {

  TextEditingController ssidController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  String changeSSID = "", changePassword = "";

  String link = "";

  List<WiFiAccessPoint> accessPoints = <WiFiAccessPoint>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            "InfyIOT"
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Enter Wifi User Name",
                  labelText: "Wifi Name"
                ),
                controller: ssidController,
              ),
              SizedBox(height: 20),
              TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Enter Password",
                    labelText: "Password"
                ),
                controller: passwordController,
              ),
              SizedBox(height: 20),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                   onPressed: () async {
                    changePassword = passwordController.text;
                    changeSSID = ssidController.text;
                    setState(() {
                      link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=1&i1ce=1&ip02=100&i2ce=1&ip03=1&i3ce=1&ddur=1&rest=1";
                    });

                    //link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=100&i1ce=1&ip02=100&i2ce=1&ip03=100&i3ce=1&ddur=1&rest=1";
                    print("############################################");
                    print(link);
                    postData(link);
                    print("############################################");
                  },
                  child: const Text(
                    "POST"
                  ),
                ),
              ),
              SizedBox(height: 20),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  onPressed: () async {
                    changePassword = passwordController.text;
                    changeSSID = ssidController.text;
                    setState(() {
                      link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=1&i1ce=1&ip02=100&i2ce=1&ip03=1&i3ce=1&ddur=1&rest=1";
                    });

                    //link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=100&i1ce=1&ip02=100&i2ce=1&ip03=100&i3ce=1&ddur=1&rest=1";
                    print("############################################");
                    print(link);
                    getData(link);
                    print("############################################");
                  },
                  child: const Text(
                      "GET"
                  ),
                ),
              ),
              SizedBox(height: 20),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  onPressed: () async {
                    changePassword = passwordController.text;
                    changeSSID = ssidController.text;
                    setState(() {
                      link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=1&i1ce=1&ip02=100&i2ce=1&ip03=1&i3ce=1&ddur=1&rest=1";
                    });

                    //link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=100&i1ce=1&ip02=100&i2ce=1&ip03=100&i3ce=1&ddur=1&rest=1";
                    print("############################################");
                    print(link);
                    putData(link);
                    print("############################################");
                  },
                  child: const Text(
                      "PUT"
                  ),
                ),
              ),
              SizedBox(height: 20),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  onPressed: () async {
                    changePassword = passwordController.text;
                    changeSSID = ssidController.text;
                    setState(() {
                      link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=1&i1ce=1&ip02=100&i2ce=1&ip03=1&i3ce=1&ddur=1&rest=1";
                    });

                    //link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=100&i1ce=1&ip02=100&i2ce=1&ip03=100&i3ce=1&ddur=1&rest=1";
                    print("############################################");
                    print(link);
                    headData(link);
                    print("############################################");
                  },
                  child: const Text(
                      "HEAD"
                  ),
                ),
              ),
              SizedBox(height: 20),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  onPressed: () async {
                    changePassword = passwordController.text;
                    changeSSID = ssidController.text;
                    setState(() {
                      link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=1&i1ce=1&ip02=100&i2ce=1&ip03=1&i3ce=1&ddur=1&rest=1";
                    });

                    //link = "http://192.168.4.1/?ssid=${changeSSID}&pswd=${changePassword}&url1=demo.isenzr.com&prt1=8883&ptp1=SensorData&ptp2=AlertData&ptp3=CommandResponse&stp1=GatewayCommand&ip01=100&i1ce=1&ip02=100&i2ce=1&ip03=100&i3ce=1&ddur=1&rest=1";
                    print("############################################");
                    print(link);
                    patchData(link);
                    print("############################################");
                  },
                  child: const Text(
                      "PATCH"
                  ),
                ),
              ),
              SizedBox(height: 20),
              Container(
                child: Text(
                  link,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  postData(String httpLink) {
      if(httpLink != ""){
        var client = http.Client();
        var uri = Uri.parse(httpLink);
        client.post(uri);
        print("Data Sent Post");
      }
  }

  getData(String httpLink) {
    // var client = http.Client();
    // var uri = Uri.parse(link);
    // var response = await client.get(uri);
    // print("Data Resived");
    // Fluttertoast.showToast(
    //     msg: "${response.statusCode}",
    //     toastLength: Toast.LENGTH_SHORT,
    //     gravity: ToastGravity.CENTER,
    //     timeInSecForIosWeb: 5,
    //     backgroundColor: Colors.red,
    //     textColor: Colors.white,
    //     fontSize: 16.0
    // );
    if(httpLink != ""){
      var client = http.Client();
      var uri = Uri.parse(httpLink);
      client.get(uri);
      print("Data Sent Get");
    }
  }

  putData(String httpLink) {
    if(httpLink != ""){
      var client = http.Client();
      var uri = Uri.parse(httpLink);
      client.put(uri);
      print("Data Sent");
    }
  }

  headData(String httpLink) {
    if(httpLink != ""){
      var client = http.Client();
      var uri = Uri.parse(httpLink);
      client.head(uri);
      print("Data Sent");
    }
  }

  patchData(String httpLink) {
    if(httpLink != ""){
      var client = http.Client();
      var uri = Uri.parse(httpLink);
      client.patch(uri);
      print("Data Sent");
    }
  }

}
